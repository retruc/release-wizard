# Release wizard

A Node.js package release wizard.

## About

This cli tool provides a way to manage releases and prereleases of Node.js projects. The only prerequisites are:
- `package.json` in the root of the project.
- Git setup in project.

This tool handles automatically package version updates, `CHANGELOG.md`, and release and prerelease commits.

## How to install

Just npm install it:

```bash
npm install --save-dev release-wizard
```

Or use it directly with npx:

```bash
npx release-wizard
```
## Usage

So far there are no usage options

```bash
npx release-wizard
```