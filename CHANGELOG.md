# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.1.1](https://gitlab.com/retruc/release-wizard/compare/v2.1.0...v2.1.1) (2021-12-29)


### Bug Fixes

* use 4 spaces in package.json autoformatting ([8f48387](https://gitlab.com/retruc/release-wizard/commit/8f483874b0c452d3df84667edb4b5647d828b232))

## [2.1.0](https://gitlab.com/retruc/release-wizard/compare/v2.0.1...v2.1.0) (2021-12-29)


### Features

* add automatic package version git refs substitution ([b1fcaa1](https://gitlab.com/retruc/release-wizard/commit/b1fcaa173892a274f389b0e33ab4960963fcb732))

### [2.0.1](https://gitlab.com/retruc/release-wizard/compare/v2.0.0...v2.0.1) (2021-12-07)

## [2.0.0](https://gitlab.com/retruc/release-wizard/compare/v1.0.3...v2.0.0) (2021-12-07)


### Features

* add major release option, remove set version tag option ([1ccc00a](https://gitlab.com/retruc/release-wizard/commit/1ccc00a7a4916f6949b6f54723f0e5ce7a3ac087))
* use v prefix in tag versioning ([8a94e62](https://gitlab.com/retruc/release-wizard/commit/8a94e6225ea4d9ac6762348aacd519363e7f47b2))

### [1.0.3](https://gitlab.com/retruc/release-wizard/compare/1.0.2...1.0.3) (2021-05-19)

### [1.0.2](https://gitlab.com/retruc/release-wizard/compare/1.0.1...1.0.2) (2021-05-19)


### Bug Fixes

* **interpreter:** fix bad interpreter configuration ([99c123c](https://gitlab.com/retruc/release-wizard/commit/99c123c3713d539572669ccc97e425b2cc76b5c4))

### [1.0.1](https://gitlab.com/retruc/release-wizard/compare/1.0.0...1.0.1) (2021-05-19)


### Bug Fixes

* **cli:** include node env so script is executed using node ([764df52](https://gitlab.com/retruc/release-wizard/commit/764df525a135b34365fc6d18709738a46b4ab2aa))

## 1.0.0 (2021-05-19)


### Features

* **cli:** base cli wizard tool ([afbcc92](https://gitlab.com/retruc/release-wizard/commit/afbcc92c6817850b541dc3ae5247b84840ac89d0))
