#!/usr/bin/env node

import chalk from 'chalk';
import inquirer from 'inquirer';
import semver from 'semver';

import { log } from './lib/log.mjs';
import { execShellCommand } from './lib/shell-utils.mjs';
import { asciiWizard } from './lib/ascii-art.mjs';
import {
    updateUnversionedPackageRefs,
    getCurrentPackageVersion,
    checkValidPackageVersion,
} from './lib/package-utils.mjs';
import {
    autoreleaseVersion,
    releaseCurrentVersion,
    prepareForNextRelease,
} from './lib/release-utils.mjs';

const currentBranchCommand = 'echo $(git branch --show-current)';
const packageNameCommand = `echo $(npm -s run env echo '$npm_package_name')`;

const choiceMainAutorelease =
    'Autorelease next verion (master/main branch only)';
const choiceMainReleaseCurrent =
    'Release current version (master/main branch only)';
const choiceMainPrepareForRelease = 'Prepare manually a future release';
const choiceUnversionedRefs = 'Update unversioned package references';
const choiceCancel = 'Cancel';

async function wizard() {
    log(chalk.rgb(255, 131, 0)(asciiWizard));
    // Combine styled and normal strings
    log(
        chalk.rgb(255, 131, 0)('Welcome') +
            ' to the ' +
            chalk.hex('#DEADED').bold.underline('Node.js') +
            ' release wizard' +
            chalk.red.bold('!')
    );

    const packageName = (await execShellCommand(packageNameCommand)).trim('\n');
    const currentBranch = (await execShellCommand(currentBranchCommand)).trim(
        '\n'
    );
    const currentPackageVersion = await getCurrentPackageVersion();
    checkValidPackageVersion(currentPackageVersion);
    const currentVersion = semver.coerce(currentPackageVersion);

    const summaryText = `
    Package: ${chalk.yellowBright(packageName)}
    Branch: ${chalk.cyanBright(currentBranch)}
    Package version: ${chalk.green(currentPackageVersion)}\n`;
    log(summaryText);
    //log(chalk.rgb(123, 45, 67).underline('Underlined reddish color'));

    const question = {
        type: 'rawlist',
        name: 'action',
        message: 'What do you want to do?',
        choices: [
            choiceMainAutorelease,
            choiceMainReleaseCurrent,
            choiceMainPrepareForRelease,
            choiceUnversionedRefs,
            choiceCancel,
            new inquirer.Separator(),
        ],
    };
    const { action } = await inquirer.prompt([question]);
    switch (action) {
        case choiceMainAutorelease:
            autoreleaseVersion(currentBranch);
            break;
        case choiceMainReleaseCurrent:
            releaseCurrentVersion(
                currentBranch,
                currentPackageVersion,
                currentVersion
            );
            break;
        case choiceMainPrepareForRelease:
            prepareForNextRelease(currentPackageVersion);
            break;
        case choiceUnversionedRefs:
            updateUnversionedPackageRefs();
            break;
        default:
            break;
    }
}

wizard();
