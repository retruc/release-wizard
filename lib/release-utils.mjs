import standardVersion from 'standard-version';
import chalk from 'chalk';
import inquirer from 'inquirer';
import { log, logError } from './log.mjs';
import { execShellCommand } from './shell-utils.mjs';
import {
    updateUnversionedPackageRefs,
    getCurrentPackageVersion,
} from './package-utils.mjs';

const choiceReleaseMajor = 'Major (i.e.: x.0.0)';
const choiceReleaseMinor = 'Minor (i.e.: 1.x.0)';
const choiceReleasePatch = 'Patch (i.e.: 1.0.x)';
const choiceReleasePrerelease = 'Prerelease (i.e.: 1.0.0-alpha.x)';
const choiceCancel = 'Cancel';

function isPackageVersionPrerelease(packageVersion) {
    return (
        packageVersion.includes('-alpha') ||
        packageVersion.includes('-beta') ||
        packageVersion.includes('-rc')
    );
}

async function setTagToCurrentCommit(versionTag) {
    await execShellCommand(`git tag -d ${versionTag} >/dev/null 2>&1 || true`);
    await execShellCommand(
        `git push --delete origin ${versionTag} >/dev/null 2>&1 || true`
    );
    await execShellCommand(`git tag ${versionTag} &>/dev/null`);
    await execShellCommand(`git push origin ${versionTag} &>/dev/null`);
    log(chalk.green('✔') + ` Updated tag ${versionTag}.\n`);
}

export async function autoreleaseVersion(currentBranch) {
    if (!['master', 'main'].includes(currentBranch)) {
        logError('A release cannot be done outside "master/main" branch');
        process.exit(1);
    }
    await updateUnversionedPackageRefs();
    await standardVersion({ silent: false });
}

export async function releaseCurrentVersion(
    currentBranch,
    currentPackageVersion,
    currentVersion
) {
    if (!['master', 'main'].includes(currentBranch)) {
        logError('A release cannot be done outside "master/main" branch');
        process.exit(1);
    }
    await updateUnversionedPackageRefs();
    const skipBump = !isPackageVersionPrerelease(currentPackageVersion);
    await standardVersion({
        silent: false,
        skip: {
            bump: skipBump,
            tag: true,
        },
    });
    const versionTag = 'v' + currentVersion;
    await setTagToCurrentCommit(versionTag);
}

async function getPrereleaseType() {
    const prereleaseQuestion = 'What kind of prerelease';
    const prereleaseChoices = [
        'alpha',
        'beta',
        'rc',
        'Cancel',
        new inquirer.Separator(),
    ];
    const questionPrerelease = {
        type: 'rawlist',
        name: 'prereleaseType',
        message: prereleaseQuestion,
        choices: prereleaseChoices,
    };
    const { prereleaseType } = await inquirer.prompt([questionPrerelease]);
    if (prereleaseType === 'Cancel') {
        process.exit(0);
    }
    return prereleaseType;
}

export async function prepareForNextRelease() {
    let releaseType;
    const question = {
        type: 'rawlist',
        name: 'action',
        // eslint-disable-next-line prettier/prettier
        message: 'What kind of release?',
        choices: [
            choiceReleasePatch,
            choiceReleaseMinor,
            choiceReleaseMajor,
            choiceReleasePrerelease,
            choiceCancel,
            new inquirer.Separator(),
        ],
    };
    const { action } = await inquirer.prompt([question]);
    let prereleaseType;
    switch (action) {
        case choiceReleasePatch:
            releaseType = 'patch';
            break;
        case choiceReleaseMinor:
            releaseType = 'minor';
            break;
        case choiceReleaseMajor:
            releaseType = 'major';
            break;
        case choiceReleasePrerelease:
            prereleaseType = await getPrereleaseType();
            break;
        default:
            log('\n');
            process.exit(0);
    }
    await standardVersion({
        silent: false,
        releaseAs: releaseType,
        prerelease: prereleaseType,
        skip: {
            changelog: true,
            commit: true,
            tag: true,
        },
    });
    const currentPackageVersion = await getCurrentPackageVersion();
    await execShellCommand(
        'git commit package-lock.json package.json ' +
            `-m "chore(prerelease): ${currentPackageVersion}"`
    );
    log(chalk.green('✔') + ' committing package-lock.json and package.json');
}
