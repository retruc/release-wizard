import { readFile, writeFile } from 'fs/promises';
import path from 'path';
import semver from 'semver';
import chalk from 'chalk';

import { log, logError } from './log.mjs';
import { execShellCommand } from './shell-utils.mjs';

const packageVersionCommand = `echo $(npm -s run env echo '$npm_package_version')`;

export async function getCurrentPackageVersion() {
    return (await execShellCommand(packageVersionCommand)).trim('\n');
}

export function checkValidPackageVersion(packageVersion) {
    if (!semver.valid(packageVersion)) {
        logError(
            `Invalid package version found, "${packageVersion}" does not follow the semver conventions`
        );
        process.exit(1);
    }
}

export async function updateUnversionedPackageRefs() {
    log('Updating unversioned package references...');
    const packagePath = path.resolve('package.json');
    const packageRaw = await readFile(packagePath);
    const packageObj = JSON.parse(packageRaw);
    await updateDepsUnversionedRefs(packageObj.dependencies);
    await updateDepsUnversionedRefs(packageObj.devDependencies);
    await writeFile(packagePath, JSON.stringify(packageObj, undefined, 4));
    await execShellCommand('npm install');
}

async function updateDepsUnversionedRefs(dependencies) {
    for (const dependencyName in dependencies) {
        const unversionedDependencyRef = getUnversionedDependencyRef(
            dependencies[dependencyName]
        );
        if (unversionedDependencyRef) {
            const version = await getVersionFromNodeModules(dependencyName);
            dependencies[dependencyName] = dependencies[dependencyName].replace(
                '#' + unversionedDependencyRef,
                '#v' + version
            );
            log(
                chalk.green('✔') +
                    ` Updated ${dependencyName} from ${unversionedDependencyRef} to v${version}`
            );
        }
    }
}

function getUnversionedDependencyRef(dependency) {
    const dependencyVersion = dependency.split('#')[1];
    if (!dependencyVersion) {
        return null;
    }
    const prunedVersion = dependencyVersion
        .replace('^', '')
        .replace('~', '')
        .replace('x', '0');
    if (!semver.valid(prunedVersion)) {
        return dependencyVersion;
    }
    return null;
}

async function getVersionFromNodeModules(dependencyName) {
    await execShellCommand(
        `rm -r node_modules/${dependencyName} && npm install ${dependencyName}`
    );
    const packagePath = path.resolve(
        `node_modules/${dependencyName}/package.json`
    );
    const packageRaw = await readFile(packagePath);
    const packageObj = JSON.parse(packageRaw);
    return packageObj.version;
}
