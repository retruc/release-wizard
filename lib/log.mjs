import chalk from 'chalk';

export const log = console.log;

export function logError(error) {
    log(chalk.red.bold('\nError: ') + error + '\n');
}
